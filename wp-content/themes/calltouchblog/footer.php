</main>
	<!-- WRAPPER -->


	<!-- FOOTER -->
	<footer class="footer">
		<div class="container">
			<div class="g-row">
				<div class="g-col-d-4">
                    <div class="links">
                        <a href="/">На главную</a>
                        <a href="https://support.calltouch.ru/hc/ru">Справочная информация</a>
                        <a href="https://www.calltouch.ru/academy/events/">Мероприятия</a>
                    </div>

					<address>
                        <ul  class="footer__list call_phone_1_1">
							<!-- <li><a href="tel:+7 495 989-47-68">+7 495 989-47-68</a></li>
							<li><a href="tel:8 800 989-47-68">8 800 989-47-68</a></li> -->
                            <li><a href="tel:+74952415811">+7 495 241-58-11</a></li>
                            <li><a href="tel:88002221381">8 800 222-13-81</a></li>
						</ul>
						<p>Calltouch &nbsp;Россия, 123557, г. Москва, <br> Пресненский вал 27с9</p>
					</address>
					<div class="social">
	                    <p>Присоединяйтесь к нам в социальных сетях</p>
	                    <ul>
	                        <li><a href="https://www.facebook.com/CallTouch" class="social__item social__fb" target="_blank"></a></li>
	                        <li><a href="https://vk.com/calltouch" class="social__item social__vk" target="_blank"></a></li>
	                        <li><a href="https://www.youtube.com/c/calltouchru" class="social__item social__tw" target="_blank"></a></li>
	                        <li><a href="https://www.slideshare.net/calltouch_group" class="social__item social__od" target="_blank"></a></li>
	                    </ul>
                	</div>
				</div>
				<div class="g-col-d-2">
                	<h4>О СЕРВИСЕ</h4>
                	<ul class="footer__menu">
                    	<li><a href="https://www.calltouch.ru/product/analiticheskaya-platforma/">Возможности</a></li>
                    	<li><a href="https://www.calltouch.ru/product/kontekstnaya-reklama/">Применение</a></li>
                    	<li><a href="https://www.calltouch.ru/pricing/">Цены</a></li>
                    	<li><a href="https://www.calltouch.ru/partners/">Партнерам</a></li>
            		</ul>
            	</div>
				<div class="g-col-d-2">
                	<h4>МЕРОПРИЯТИЯ</h4>
                	<ul class="footer__menu">
                    	<li><a href="https://www.calltouch.ru/academy/events/webinar/">Вебинары</a></li>
                    	<li><a href="https://www.calltouch.ru/academy/events/conference/">Конференции</a></li>
                    	<li><a href="https://www.calltouch.ru/academy/events/offline/">Оffline мероприятия</a></li>
                    	<li><a href="https://www.calltouch.ru/academy/events/callday/">Callday</a></li>
            		</ul>
            	</div>
				<div class="g-col-d-2">
                	<h4>БАЗА ЗНАНИЙ</h4>
                	<ul class="footer__menu">
                    	<li><a href="https://support.calltouch.ru/hc/ru/categories/200804899-%D0%98%D0%BD%D1%82%D0%B5%D0%B3%D1%80%D0%B0%D1%86%D0%B8%D0%B8" target="_blank">Интеграции</a></li>
                    	<li><a href="https://support.calltouch.ru/hc/ru/categories/200944555-%D0%9D%D0%B0%D1%81%D1%82%D1%80%D0%BE%D0%B9%D0%BA%D0%B8" target="_blank">Настройка системы</a></li>
                    	<li><a href="https://support.calltouch.ru/hc/ru/categories/200804919-%D0%9E%D1%82%D1%87%D0%B5%D1%82%D1%8B" target="_blank">Отчеты</a></li>
                    	<li><a href="https://support.calltouch.ru/hc/ru" target="_blank">Справочный центр</a></li>
            		</ul>
            	</div>
				<div class="g-col-d-2">
                	<h4>О КОМПАНИИ</h4>
               		<ul class="footer__menu">
                    	<li><a href="https://www.calltouch.ru/about/press-list/">Пресса о нас</a></li>
                    	<li><a href="https://www.calltouch.ru/about/vacancies/">Вакансии</a></li>
                    	<li><a href="https://www.calltouch.ru/about/contacts">Контакты</a></li>
            		</ul>
            	</div>
			</div>
			<div class="g-row">
            <div class="g-col-d-4"></div>
            <div class="g-col-d-6">
                <ul class="footer__links">
                    <li><a href="https://www.calltouch.ru/offer-soft/" class="blue-link" target="_blank">Лицензионный договор</a></li>
                    <li>
                        <a href="https://www.calltouch.ru/privacy_policy.pdf" class="blue-link" target="_blank">Политика в отношении обработки персональных данных</a>
                    </li>
                    <li><a href="https://www.calltouch.ru/offer-link/" class="blue-link" target="_blank">Договор связи</a></li>
                    <li>
                        <a href="https://www.calltouch.ru/agreement/" class="blue-link" target="_blank">Согласие на обработку персональных данных</a>
                    </li>
                </ul>
            </div>
            <div class="g-col-d-2">
                <div class="footer__create">Разработка сайта <a href="http://www.ibrush.ru/" class="blue-link" target="_blank">iBrush</a>
                </div>
            </div>
        </div>
		</div>
	</footer>
	<!-- /FOOTER -->

    <!-- Rating@Mail.ru counter --> 
    <script type="text/javascript"> 
    var _tmr = window._tmr || (window._tmr = []); 
    _tmr.push({id: "2739021", type: "pageView", start: (new Date()).getTime()}); 
    (function (d, w, id) { 
    if (d.getElementById(id)) return; 
    var ts = d.createElement("script"); ts.type = "text/javascript"; ts.async = true; ts.id = id; 
    ts.src = (d.location.protocol == "https:" ? "https:" : "http:") + "//top-fwz1.mail.ru/js/code.js"; 
    var f = function () {var s = d.getElementsByTagName("script")[0]; s.parentNode.insertBefore(ts, s);}; 
    if (w.opera == "[object Opera]") { d.addEventListener("DOMContentLoaded", f, false); } else { f(); } 
    })(document, window, "topmailru-code"); 
    </script><noscript><div style="position:absolute;left:-10000px;"> 
    <img src="//top-fwz1.mail.ru/counter?id=2739021;js=na" style="border:0;" height="1" width="1" alt="Рейтинг@Mail.ru" /> 
    </div></noscript> 
<!-- //Rating@Mail.ru counter -->

<script> 
    (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){ 
    (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o), 
    m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m) 
    })(window,document,'script','https://www.google-analytics.com/analytics.js','ga'); 
    
    ga('create', 'UA-26344020-1', 'auto'); 
    ga('send', 'pageview'); 
</script>

<script>
    (function(w, c) {
    (w[c] = w[c] || []).push(function() {
    try {
    w.yaCounter12606739 = new Ya.Metrika({id:12606739, enableAll: true, webvisor:true, triggerEvent:true});
    }
    catch(e) { }
    });
    })(window, "yandex_metrika_callbacks");
</script>

<script>
    (function() {
    var _fbq = window._fbq || (window._fbq = []);
    if (!_fbq.loaded) {
    var fbds = document.createElement('script');
    fbds.async = true;
    fbds.src = '//connect.facebook.net/en_US/fbds.js';
    var s = document.getElementsByTagName('script')[0];
    s.parentNode.insertBefore(fbds, s);
    _fbq.loaded = true;
    }
    _fbq.push(['addPixelId', '655668371205492']);
    })();
    window._fbq = window._fbq || [];
    window._fbq.push(['track', 'PixelInitialized', {}]);
</script>

<!-- Facebook Pixel Code -->
<script>
!function(f,b,e,v,n,t,s){if(f.fbq)return;n=f.fbq=function(){n.callMethod?
n.callMethod.apply(n,arguments):n.queue.push(arguments)};if(!f._fbq)f._fbq=n;
n.push=n;n.loaded=!0;n.version='2.0';n.queue=[];t=b.createElement(e);t.async=!0;
t.src=v;s=b.getElementsByTagName(e)[0];s.parentNode.insertBefore(t,s)}(window,
document,'script','https://connect.facebook.net/en_US/fbevents.js');
fbq('init', '655668371205492');
fbq('track', 'PageView');
</script>
<noscript><img height="1" width="1" style="display:none"
src="https://www.facebook.com/tr?id=655668371205492&ev=PageView&noscript=1"
/></noscript>
<!-- DO NOT MODIFY -->
<!-- End Facebook Pixel Code -->


<?php wp_footer(); ?>

 <script>
 	document.addEventListener("DOMContentLoaded", function(){
 		document.querySelectorAll(".events__nav-btn").forEach(function(item){
 			item.addEventListener("click", function(e){
 				e.target.parentNode.parentNode.classList.add("hover");
 			});
 		});


 	});
 </script>
<link rel='stylesheet' id='main-style' href='<?php echo get_stylesheet_uri(); ?>' type='text/css' media='all' />

<?php /*if (!isset($_COOKIE['notice'])) { ?>
    <? setcookie('notice', 1); ?>
    <script>
        function deletePopup() {
            document.getElementById('cookie-notice').remove();
        }
    </script>

    <div id="cookie-notice" class="cookie-notice">
        <div class="container">Продолжая использовать наш сайт, вы даете согласие на обработку файлов cookie, пользовательских данных (сведения о местоположении; тип и версия ОС; тип и версия Браузера; тип устройства и разрешение его экрана; источник откуда пришел на сайт пользователь; с какого сайта или по какой рекламе; язык ОС и Браузера; какие страницы открывает и на какие кнопки нажимает пользователь; ip-адрес) в целях функционирования сайта, проведения ретаргетинга и проведения статистических исследований и обзоров. Если вы не хотите, чтобы ваши данные обрабатывались, покиньте сайт.
        
        <button title="Close (Esc)" type="button" onclick="deletePopup()">×</button></div>
    </div>
<?php } */?>

<!-- Yandex.Metrika counter --> <script type="text/javascript" > (function (d, w, c) { (w[c] = w[c] || []).push(function() { try { w.yaCounter46298718 = new Ya.Metrika({ id:46298718, clickmap:true, trackLinks:true, accurateTrackBounce:true, webvisor:true, triggerEvent:true }); } catch(e) { } }); var n = d.getElementsByTagName("script")[0], s = d.createElement("script"), f = function () { n.parentNode.insertBefore(s, n); }; s.type = "text/javascript"; s.async = true; s.src = "https://mc.yandex.ru/metrika/watch.js"; if (w.opera == "[object Opera]") { d.addEventListener("DOMContentLoaded", f, false); } else { f(); } })(document, window, "yandex_metrika_callbacks"); </script> <noscript><div><img src="https://mc.yandex.ru/watch/46298718" style="position:absolute; left:-9999px;" alt="" /></div></noscript> <!-- /Yandex.Metrika counter -->

</body>
</html>