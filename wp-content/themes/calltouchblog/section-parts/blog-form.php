<div class="blue-form">
    <div class="container" id="blog-subscribe">
        <form class="form blue-form__body has-white-txt blog-subscribe-form"  target="_blank" class="" name="mc-embedded-subscribe-form" id="mc-embedded-subscribe-form" novalidate="novalidate" method="post" action="//calltouch.us13.list-manage.com/subscribe/post?u=b50f4659256a484689b1f036e&amp;id=9d28a09def">
            <h4 class="blue-form__title">Оставайтесь в курсе событий</h4>
            <div class="blue-form__note">Новости маркетинга, новые инструменты и секреты продвижения с доставкой в вашу почту</div>
            <div class="form__line">
                <label class="form__input js-input-field">
                    <input name="EMAIL" type="text">
                    <span>Ваш e-mail</span>
                </label>
            </div>
            <div class="form__line mod-top-m40">
                <input name="sessid" id="sessid" value="b588caa3170a8a4c40521bbfe944ef80" type="hidden">
                <input name="form_id" value="13" type="hidden">
                <input class="btn btn_yellow-big" value="Подписаться" type="submit">
            </div>
            <span class="ready-use-personal-notice personal-notice">Нажимая на кнопку "Подписаться", вы даёте своё согласие на <a href="https://www.calltouch.ru/agreement/" target="_blank">обработку персональных данных</a></span>
        </form>
    </div>
</div>