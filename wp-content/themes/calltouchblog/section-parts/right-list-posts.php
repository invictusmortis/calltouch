<!-- ARTICLE SIDE -->
<aside class="article__side">
	<h4 class="article__side-title">Популярные материалы</h4>

	<div class="article__side-news-wrapper">
		<?php 
	
			$postId = get_the_ID();

			$args = array(
				'post__not_in' => array($postId),
				'posts_per_page' => 4,
                'meta_key' => 'views',
				'orderby' => 'meta_value_num',
                'order' => 'DESC'
			);
	
	
			$pc = new WP_Query($args);
			while ($pc->have_posts()) : $pc->the_post();
	
			// if($postId == get_the_ID()) continue;
	
		?>
	
			<div class="side-news article__side-news">
				<div class="side-news__thumbnail"><a href="<?=the_permalink();?>"><?=the_post_thumbnail(); ?></a></div>

				<h5 class="side-news__title">
					<a href="<?=the_permalink();?>"><?=the_title()?></a>
				</h5>

				<ul class="side-news-info">
					<li class="side-news-info__item">
						<div class="side-news__date"><?=get_the_modified_date('d F Y')?></div>
					</li>
					<li class="side-news-info__item">
						<?php if($sView = get_post_meta($post->ID, "views", true)) {?>
		                    <div class="eye"><?=$sView;?></div>

		                <?php }?>
	                </li>
                </ul>
			
	
				<?php 
					$tags = the_tags();
				?>
	
				<ul class="tags-list side-news__tags">
					<?php foreach($tags as $tag): ?>
						<li><a href="#"><?=$tag->name?></a></li>
					<?php endforeach;?>	
				</ul>
			</div>
	
		<?php 
			endwhile;
		?>
	</div>

</aside>
<!-- /ARTICLE SIDE -->