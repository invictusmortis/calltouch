<?php
/*var_dump($post);*/

$sTags = "";
if($obTags = wp_get_post_tags($post->ID)) {
    foreach ($obTags as $obTag) {
        $sTags .= $obTag->name.", ";
    }
    $sTags = substr($sTags, 0, -2);
}

if($obCategory = get_the_category($post->ID)) {
    $sCategory = "&nbsp;&nbsp;|&nbsp;&nbsp;<a class='category-name' href='/".$obCategory[0]->taxonomy."/".$obCategory[0]->slug."/'>".$obCategory[0]->name."</a>&nbsp;&nbsp;|&nbsp;&nbsp";
}
?>
<!-- PAGE-BODY -->
<div class="page__body">
	<div class="article">
		<div class="container">
			<div class="g-row">
				<!-- ARTICLE BODY -->
				<div class="article__body">
					<!-- USER_CONTENT -->
					<div class="user-content-styles">
                        <div class="top-stat">
                            <?=get_the_date('d F Y', $post->ID);?><?php if($sCategory) echo $sCategory;?>

                            <?php if($sView = get_post_meta($post->ID, "views", true)) {?>
                                <div class="eye"><?=$sView;?></div>
                                
                            <?php }?>
                        </div>

                        <?=$post->post_content;?>
                        <?php
                        $obTags = wp_get_post_tags($post->ID);
                        if($obTags) {
                            ?>
                            <ul class="tags-list articles-unit__links">
                                <?php
                                foreach($obTags as $cat) {
                                    if(strpos($cat->name, " ") !== false) {
                                        $sTag = $cat->slug;
                                    } else {
                                        $sTag = $cat->name;
                                    }
                                    ?>
                                    <li>
                                        <a href="/?s=<?=urldecode($cat->name);?>&id=<?=$cat->term_id;?>"><?=$cat->name;?></a>
                                    </li>
                                <?php }?>
                            </ul>
                        <?php }?>


					</div>
					<!-- /USER_CONTENT -->
                    
                    <?php if(function_exists("kk_star_ratings")) : echo kk_star_ratings($post->ID); endif; ?>
                    <?php
						// If comments are open or we have at least one comment, load up the comment template.
						if ( comments_open() || get_comments_number() ) :
							comments_template();
						endif;
						?>
                    
				</div>
				<!-- /ARTICLE BODY -->
				<?php get_template_part("section-parts/right-list-posts");?>
			</div>
		</div>
	</div>
</div>
<!-- /PAGE-BODY -->

<?php get_template_part("section-parts/blog-form");?>