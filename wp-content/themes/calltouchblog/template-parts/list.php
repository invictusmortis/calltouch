<div class="page__body">
	<div class="container">

		<!-- ARTICLES -->
		<div class="articles">

		<?php 

			$pageUrl = get_permalink();
			$pageTitle = get_the_title();

			/*echo "<pre>";
			print_r($pageTitle);
			die;*/

			global $wp_query;
			global $paged;

			if(get_query_var( 'page' )){
				$paged = get_query_var( 'page' );
			}else if(get_query_var( 'paged' )){
				$paged = get_query_var( 'paged' );
			}else{
				$paged = 1;
			}

			$cat = 0;

			if(is_category()){
				$cat = get_queried_object()->term_id;
			}

			$args = array(
				'posts_per_page' => $wp_query->query_vars['posts_per_page'],
			   	'paged' => $paged,
				'numberposts' => $wp_query->query_vars['posts_per_page'],
				'category'    => $cat,
				'orderby'     => 'post_date',
				'order'       => 'DESC',
				"tag" => $_GET['tag'],
                "post_status" => "publish"
			);

			
           // echo count($custom_query->posts);
			$posts = get_posts($args);
            //echo count();

			/*if($_COOKIE["test"]) {
			    echo "<pre>";
			    print_r($posts);
			    echo "</pre>";
            }*/




		?>
			<?php if(count($posts)): ?>
				<?php foreach($posts as $post):
                    if($obCategory = get_the_category($post->ID)) {
                        $sCategory = "&nbsp;&nbsp;|&nbsp;&nbsp;";
                        $sCategory .= "<a href='/".$obCategory[0]->taxonomy."/".$obCategory[0]->slug."/'>".$obCategory[0]->name."</a>&nbsp;&nbsp;";
                    }

                    $sTags = "";
                    if($obTags = wp_get_post_tags($post->ID)) {
                        foreach ($obTags as $obTag) {
                            $sTags .= $obTag->name.", ";
                        }
                        $sTags = substr($sTags, 0, -2);
                    }
                    ?>
					<?php
						$thumbnail = get_the_post_thumbnail($post->ID, Array(370, 247), array(
							'class' => "articles-unit__thumb-img",
							'alt'   => trim(strip_tags( $post->post_title )),
							'title' => trim(strip_tags( $post->post_title )),
						));
					?>

					<div class="articles-unit">
						<div class="articles-unit__thumb">
							<a class="articles-unit__thumb-link" href="<?=the_permalink()?>">
								<?=$thumbnail?>
							</a>
						</div>
						<div class="articles-unit__details">
							<div class="articles-unit__text">
								<div class="articles-unit__date">
                                    <?=get_the_date('d F Y', $post->ID);?><?=$sCategory;?>
                                    <?php

                                    if($sView = get_post_meta($post->ID, "views", true)) {

                                    ?>
                                        |&nbsp;&nbsp;<div class="eye"><?=$sView;?></div>
                                    <?php }?>
                                </div>
								<div class="articles-unit__title">
									<a href="<?=the_permalink()?>"><?=trim(strip_tags( $post->post_title ))?></a>
								</div>
								<div class="articles-unit__desc">
									<a href="<?=the_permalink()?>"><?=trim(strip_tags( $post->post_excerpt ))?></a>
								</div>
							</div>


                            <?php
                            if($obTags) {
                            ?>
                                <ul class="tags-list articles-unit__links">
                                    <?php
                                    foreach($obTags as $cat) {
                                        if(strpos($cat->name, " ") !== false) {
                                            $sTag = $cat->slug;
                                        } else {
                                            $sTag = $cat->name;
                                        }
                                        ?>
                                        <li>
                                            <a href="/?s=<?=urldecode($cat->name);?>&id=<?=$cat->term_id;?>"><?=$cat->name;?></a>
                                        </li>
                                    <?php }?>
                                </ul>
						    <?php }?>
						</div>
					</div>

				<?php endforeach;?>	

				<?php if (function_exists("pagination")) {
					if(!is_category()){
                        $custom_query = new WP_Query( $args );
						pagination($custom_query->max_num_pages);
					}else{
						//$catData = get_category($cat); print_r($catData);
						pagination();
					}
				    
				} ?>

			<?php else: ?>
				
				<?php emptyResult();?>

			<?php endif;?>	


		</div>

		

		<!-- ARTICLES -->
	</div>

</div>
<!-- /PAGE-BODY -->